# Задание
# Опишите свой класс исключения.
# Напишите функцию, которая будет выбрасывать данное исключение,
# если пользователь введёт определённое значение, и перехватите это исключение при вызове функции.
# Самостоятельная деятельность учащегося

# Задание 1
# Выучите основные стандартные исключения, которые перечислены в данном уроке.

# Задание 2
# Напишите программу-калькулятор, которая поддерживает следующие операции:
#  сложение, вычитание, умножение, деление и возведение в степень.
# Программа должна выдавать сообщения об ошибке и продолжать работу при вводе некорректных данных,
# делении на ноль и возведении нуля в отрицательную степень.


class Calc:
    def __init__(self):
        self.__a = 0
        self.__b = 0
        self.__opr = 0

    def set_a(self, a):
        self.__a = a

    def set_b(self, b):
        self.__b = b

    def __set_val(self):
        self.__a = float(input('Enter a '))
        self.__b = float(input('Enter b '))

    def operation(self):
        while self.__opr != 'exit':
            try:
                self.__opr = str(input('Enter { + - * / or Exit } : '))
                if self.__opr == '+':
                    self.__set_val()
                    print(self.__addition())
                elif self.__opr == '-':
                    self.__set_val()
                    print(self.__subtraction())
                elif self.__opr == '*':
                    self.__set_val()
                    print(self.__multiplication())
                elif self.__opr == '/':
                    self.__set_val()
                    print(self.__division())
                elif self.__opr == 'exit':
                    return 0
            except ValueError as error:
                print('Exception:', error)

    def __addition(self):
        return self.__a + self.__b

    def __subtraction(self):
        return self.__a - self.__b

    def __multiplication(self):
        return self.__a * self.__b

    def __division(self):
        if self.__b == 0:
            return 'Error'
        else:
            return self.__a / self.__b


def main():
    calc = Calc()
    calc.operation()


if __name__ == '__main__':
    main()

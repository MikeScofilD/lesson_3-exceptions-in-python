# Задание 3
# Опишите класс сотрудника,
# который включает в себя такие поля, как имя,
# фамилия, отдел и год поступления на работу.
#  Конструктор должен генерировать исключение,
# если заданы неправильные данные.
#  Введите список работников с клавиатуры.
# Выведите всех сотрудников, которые были приняты после заданного года.


class Employee:
    def __init__(self, name, surname, department, year):
        if not name:
            raise ValueError('Name cannot be empty! ')
        if not surname:
            raise ValueError('Surname cannot be empty! ')
        if not department:
            raise ValueError('Department cannot be empty! ')
        if not year:
            raise ValueError('Year cannot be empty! ')

        self.name = name
        self.surname = surname
        self.department = department
        self.year = year

    def year_corrected(self):
        years = input(str('Enter the employees year of employment'))
        if self.year >= years:
            return f"Name:{self.name} " \
                   f"Surname: {self.surname} " \
                   f"Department:{self.department} " \
                   f"Year:{self.year}"
        else:
            return 0

    def __str__(self):
        return f"Name:{self.name} " \
               f"Surname: {self.surname} " \
               f"Department:{self.department} " \
               f"Year:{self.year}"


def create_employee():
    create = 'y'
    new_employee = []
    while create == 'y':
        name = input(str('Enter name Employee: '))
        surname = input(str('Enter Surname Employee: '))
        department = input(str('Enter Department Employee: '))
        year = int(input("Enter Year Employee: "))
        try:
            employee = Employee(name, surname, department, year)
            new_employee.append(employee)
        except ValueError as error:
            print('Exception:', error)
        create = input(str('Create new Employee?? y/n '))

    emp_year = int(input("Enter the employee's year of employment"))
    for employee in new_employee:
        if employee.year >= emp_year:
            print(employee)


def main():
    create_employee()


if __name__ == '__main__':
    main()
